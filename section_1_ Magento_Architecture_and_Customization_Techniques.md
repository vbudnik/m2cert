<h1>Magento Architecture and Customization Techniques</h1>

<h2>1.1. Describe Magento’s module-based architecture</h2>
<p>
    Magento 2 modules are realized with MVVM architecture:
</p>

![Alt text](./image/2-tutorial.jpg?raw=true) <br><br>
_MVVM has three layers:_
1. **Model**<br>
The Model contains the application’s business logic and depends on an associated class—the ResourceModel – for database access. Models depend on service contracts to disclose their functionality to other application layers.

2. **View**<br>
The View is both structure and layout of what is seen on a screen – the actual HTML. This is achieved in the PHTML files distributed with modules. Such files are associated with each ViewModel in the Layout XML files, sometimes referred to as binders. The layout files can as well assign JavaScript files to be used on the final page.

3. **ViewModel**<br>
The ViewModel works together with the Model layer and exposes only necessary information to the View layer. In Magento 2, this is handled by the module’s Block classes. Note that this was usually part of the Controller role of an MVC system. On MVVM, the controller is only responsible for handling the user flow, meaning that it receives requests and either tells the system to render a view or to redirect the user to another route.

![Alt text](./image/1-tutorial.jpg?raw=true)<br><br>
_Magento 2 architecture consists of 4 layers:_
1. **Presentation Layer**<br>
Presentation Layer is the top layer that contains view elements (layouts, blocks, templates) and controllers.
Presentation Layer usually calls the service layer using service contracts. But, depending on the implementation, it may cross over with the business logic

2. **Service layer**<br>
Service layer is the layer between presentation and domain layers. It executes service contracts, which are implemented as PHP interfaces. Service contracts allow to add or change the business logic resource model using the dependency injection file (di.xml). The service layer is also used to provide API access (REST / SOAP or other modules). The service interface is declared in the / API namespace of the module.
Data (entity) interface is declared inside / Api / Data. Data entities are the structures of data passed to and returned from service interfaces.

3. **Domain layer**<br>
The domain layer is responsible for business logic that does not contain information about resources and the database. Also, the domain layer may include the implementation of service contracts. Each data model at the domain layer level depends on the resource model, which is responsible for the database access.

4. **Persistence layer**<br>
The persistence layer describes a resource model that is responsible for retrieving and modifying data in a database using CRUD requests.
It also implements additional features of business logic, such as data validation and the implementation of database functions.


<h3>Describe module limitations</h3>
1. Certain modules can come in conflict with each other if the dependencies in module.xml (sequence) are specified incorrectly.<br>

2. Not all classes can be overridden with modules.<br>

<h2>1.2 Describe Magento’s directory structure</h2>

**Determine how to locate different types of files in Magento. Where are the files containing JavaScript, HTML, and PHP located?**

![Alt text](./image/Magento-2-Certified-Professional-Developer-Guide-Screenshot-1.jpg?raw=true)<br><br>

The whole Magento structure can be divided into the following types:

1. Magento root structure<br>
2. Modules structure<br>
3. Themes structure<br>

To begin with, Magento has various areas that allow to determine configuration, view files, etc. for a certain area. Adminhtml (applied to the administration panel) and frontend (applied to frontend parts of the website) are examples of area. From this point, we will use <**area**> to denote any of the available areas.

Magento root partition structure:

1. _app_ – the directory recommended for Magento components development. It consists of:<br>
    `a`. _design_ – contains themes<br>
    `b`. _code_ – contains themes<br>
    `c`. _etc_ – contains themes<br>
    `d`. _i18n_ – contains themes<br>
    ![Alt text](./image/Magento-2-Certified-Professional-Developer-Guide-Screenshot-2.jpg?raw=true)<br><br>
2. _bin_ – there the executed Magento file is located that allows to manage the website via CLI.<br>
3. _dev_ –  the directory for Magento test scripts (for more information, follow the link https://devdocs.magento.com/guides/v2.3/mtf/mtf_quickstart.html).

4. _generated_ – contains the generated php files (Factories, Proxies, Interceptors, DI configuration).<br>
5. _lib_ – used for Magento library files.<br>

6. _phpserver_ – contains the router file “router.php” for the php Built-in web server. Allows to use Magento without a third-party web server, like nginx and apache. Here is the example of php Built-in web server launch: **php -S 127.0.0.1:8082 -t ./pub/ ./phpserver/router.php**<br>
7. _pub_ – the directory used to access Magento static files. It contains the following directories:<br>
    `1`._errors_ – stores the error pages,<br>
    `2`._media_ – stores all media files (product, pages, etc.),<br>
    `3`._static_ – stores Magento themes generated files.This directory can be specified as web root in nginx config or in apache config. Numerous Magento directories contain “.htaccess” files (including root and pub), which allow you to configure apache for a specific directory. Nginx does not support .htaccess. For nginx, Magento has a nginx.conf.sample file, which is an example of Magento configuration for nginx. This file can be copied, modified, and include the main nginx configuration file.<br>

8. _setup_ – contains Setup Wizard<br>

9. _update_ –contains Magento Updater<br>
10. _var_ – contains all the temporary files. Consists of:<br>
        ![Alt text](./image/Magento-2-Certified-Professional-Developer-Guide-Screenshot-3.jpg?raw=true)<br><br>
    `1`. _cache_ – contains cache files if cache utilizes file system as a storage<br>
    `2`. _page_cache_ – contains FPC (Full Page Cache) files, if FPC utilizes file system as a storage<br>
    `3`. _log_ – contains Magento logs<br>
    `4`. _report_ – contains Magento error/exception files that were not intercepted by code<br>
    `5`. _session_ – contains session files<br>
    `6`. _view_preprocessed_ – contains style generated files and minified HTML<br>
